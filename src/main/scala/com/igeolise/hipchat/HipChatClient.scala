package com.igeolise.hipchat

import HipChatClient._
import HttpHipChatClient._
import dispatch._
import play.api.libs.json._

import scala.concurrent.ExecutionContext.Implicits.global

trait HipChatClient {
  def send(msg: String, color: Color, notify: Boolean): Future[Throwable Either Unit]
}

object HipChatClient {
  sealed trait Color
  case object Yellow extends Color
  case object Green extends Color
  case object Red extends Color
  case object Purple extends Color
  case object Gray extends Color
  case object Random extends Color
}

/**
  * https://www.hipchat.com/docs/apiv2/method/send_room_notification
  */
class HttpHipChatClient(val roomId: String, val authToken: String) extends HipChatClient {

  private[this] def notificationUrl = url(s"$ApiUrl/v2/room/$roomId/notification").
    addQueryParameter("auth_token", authToken)

  def send(msg: String, color: Color, notify: Boolean): Future[Throwable Either Unit] = {
    val request = Json.obj(
      "color" -> colorString(color),
      "message_format" -> "html",
      "message" -> {
        if (msg.length > MaxMessageLength) msg.take(MaxMessageLength) + "..."
        else msg
      },
      "notify" -> notify
    )

    // XXX: we don't care about response as long as it is a Success
    Http(notificationUrl.postJson(request).OK(_ => ())).either
  }
}

object HttpHipChatClient {

  val ApiUrl = "https://api.hipchat.com"

  val MaxMessageLength = 9900

  def colorString: Color => String = {
    case Yellow => "yellow"
    case Green  => "green"
    case Red    => "red"
    case Purple => "purple"
    case Gray   => "gray"
    case Random => "random"
  }

  implicit class RequestBuilderExts(val rb: Req) extends AnyVal {
    def postJson(js: JsValue): Req = {
      rb.POST.addHeader("Content-Type", "application/json") << js.toString()
    }
  }
}
